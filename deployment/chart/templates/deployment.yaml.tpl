
apiVersion: apps/v1
kind: Deployment
metadata:
  name: builds-admission
  labels:
    name: builds-admission
  annotations:
    secret.reloader.stakater.com/reload: "{{ .Values.webhook.secretName }}"
spec:
  replicas: {{ .Values.replicas }}
  selector:
    matchLabels:
      name: builds-admission
  template:
    metadata:
      name: builds-admission
      labels:
        name: builds-admission
    spec:
      containers:
        - name: webhook
          image: "{{ .Values.image.name }}:{{ .Values.image.tag }}"
          imagePullPolicy: "{{ .Values.image.pullPolicy }}"
          env:
            - name: "DEBUG"
              value: "{{ .Values.config.debug }}"
            - name: "ALLOWEDDOMAINS"
              value: "{{ .Values.config.harborDomains | join "," }}"
            - name: "ALLOWEDBUILDERS"
              value: "{{ .Values.config.builders | join "," }}"
            - name: "SYSTEMUSERS"
              value: "{{ .Values.config.systemusers | join "," }}"
          resources: {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: webhook-certs
              mountPath: /etc/webhook/certs
              readOnly: true
          securityContext:
            readOnlyRootFilesystem: true
      volumes:
        - name: webhook-certs
          secret:
            secretName: {{ .Values.webhook.secretName }}
