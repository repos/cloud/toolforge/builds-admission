# Buildpack Pipeline Validation Webhook for Toolforge

This is a
[Kubernetes Admission Validation Webhook](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#what-are-admission-webhooks)
deployed to check that users are not setting task values that might use external
buliders.

## Use and development

This is pending adaption to Toolforge. Currently it depends on local docker
images and it can be built and deployed on Kubernetes by ensuring any node it is
expected to run on has access to the image it uses. The image will need to be in
a registry most likely when deployed.

**Export your local repository ip**

It will try to guess your local repository ip, but you can force one by
exporting it yourself. The IP to use will vary depending on your machine and OS,
This is usually:

- `export HARBOR_IP=$(minikube ssh "grep host.minikube.internal /etc/hosts" | awk '{print $1}')`

But sometimes (ex. debian bullseye) you will need to use the external IP of your
host:

- `export HARBOR_IP=$(hostname -I| awk '{print $1}')`

**Build on minikube**

Before you attempt to build on minikube, you need to make sure you have
cert-manager installed. you can verify this by running:
`kubectl get pods -n cert-manager` if you have cert-manager installed you will
see the pods running. If cert-manager is not installed, clone
https://gitlab.wikimedia.org/repos/cloud/toolforge/cert-manager and run the
./deploy.sh script

To build on minikube (current supported k8s version is 1.21) and launch, just
run:

```
make build-and-deploy-local
```

After you've made changes, just rerun the above command again.

**Making changes** Before you make a change, you need setup pre-commit on your
local machine.

- run `pip3 install pre-commit` on your local machine (`brew install pre-commit`
  if using homebrew)
- run `pre-commit install` to setup the git hook scripts.

After the above steps, you can go ahead and make changes, commit and push.

## Testing

At the top level, run `go test ./...` to capture all tests. If you need to see
output or want to examine things more, use `go test -test.v ./...`

## Deploying To Production

We deploy this component using the toolforge-deploy repository:

https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/
